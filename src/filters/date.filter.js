export function filterDate(dateObj) {
  return `${dateObj.getDate()}.${dateObj.getMonth()}.${dateObj.getFullYear()}`;
}
