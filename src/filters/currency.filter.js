const numberFormat = new Intl.NumberFormat("ru-RU", {
  style: "currency",
  currency: "RUB"
});
export function currencyFilter(float) {
  return numberFormat.format(float);
}
