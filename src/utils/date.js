export function DateFromString(strDate) {
  const [yyyy, mm, dd] = strDate.split("-");
  return new Date(yyyy, +mm - 1, dd);
}

/**
 * @return {string}
 */
export function DateToString(dateObj) {
  return (
    dateObj.getFullYear() + "-" + dateObj.getMonth() + "-" + dateObj.getDate()
  );
}
