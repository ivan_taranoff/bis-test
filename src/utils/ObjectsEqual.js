/**
 * @return {boolean}
 */
export function ObjectsEqual(a, b) {
  return JSON.stringify(a) === JSON.stringify(b);
}
