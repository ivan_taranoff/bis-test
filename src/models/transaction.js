import { DateFromString } from "../utils/date";

export function createTransaction({
  AcctCr = "",
  AcctDB = "",
  Amount = 0,
  OpDate = ""
}) {
  return {
    AcctCr,
    AcctDB,
    Amount,
    OpDate,
    date: (OpDate.length && DateFromString(OpDate)) || new Date(0)
  };
}
