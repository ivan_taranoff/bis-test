import { DateFromString } from "../utils/date";

export function createDay({ OpDate = "", OpStatus = "" }) {
  return {
    OpStatus,
    OpDate,
    date: (OpDate.length && DateFromString(OpDate)) || new Date(0)
  };
}
