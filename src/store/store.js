import Vue from "vue";
import Vuex from "vuex";
import { axiosInstance } from "../plugins/axios";
import { createTransaction } from "../models/transaction";
import { createDay } from "../models/day";

import { accounts, ACCOUNTS_MUTATIONS } from "./modules/accounts.store";
import {
  transactions,
  TRANSACTIONS_MUTATIONS
} from "./modules/transactions.store";
import { days, DAYS_MUTATIONS } from "./modules/days.store";

Vue.use(Vuex);

export const ROOT_ACTIONS = {
  FETCH_INITIAL: "FETCH_INITIAL",
  FETCH_DAYS: "FETCH_DAYS",
  FETCH_ACCOUNTS: "FETCH_ACCOUNTS",
  FETCH_DOCUMENTS: "FETCH_DOCUMENTS",
  FETCH_ACCOUNT_ORIGINAL_BALANCE: "FETCH_ACCOUNT_ORIGINAL_BALANCE"
};

const storeRoot = {
  state: {},
  getters: {},
  mutations: {},
  actions: {
    [ROOT_ACTIONS.FETCH_INITIAL]({ dispatch }) {
      const days = dispatch(ROOT_ACTIONS.FETCH_DAYS);
      const accounts = dispatch(ROOT_ACTIONS.FETCH_ACCOUNTS);
      const documents = dispatch(ROOT_ACTIONS.FETCH_DOCUMENTS);
      return Promise.all([days, accounts, documents]);
    },
    [ROOT_ACTIONS.FETCH_DAYS]({ commit }) {
      // available days
      return axiosInstance.get("/OpDate.json").then(({ data }) => {
        commit(
          DAYS_MUTATIONS.SET_DAYS,
          data.map(d => createDay(d))
        );
      });
    },
    [ROOT_ACTIONS.FETCH_ACCOUNTS]({ commit }) {
      // accounts
      return axiosInstance.get("/AcctAcct.json").then(({ data }) => {
        commit(ACCOUNTS_MUTATIONS.SET_ACCOUNTS, data);
      });
    },
    [ROOT_ACTIONS.FETCH_DOCUMENTS]({ commit }) {
      // documents
      return axiosInstance.get("/Doc.json").then(({ data }) => {
        commit(
          TRANSACTIONS_MUTATIONS.SET_DOCUMENTS,
          data.map(d => createTransaction(d))
        );
      });
    },
    [ROOT_ACTIONS.FETCH_ACCOUNT_ORIGINAL_BALANCE](context, accountNumber) {
      // accounts
      context;
      return axiosInstance
        .get("/AcctAcct.json?Acct=" + accountNumber)
        .then(({ data }) => data);
    }
  }
};
export function createStore() {
  let modules = {
    accounts: accounts(),
    days: days(),
    transactions: transactions()
  };
  // const requireService = require.context("./modules", false, /.store.js$/);
  // requireService.keys().forEach(filename => {
  //   const moduleName = filename.replace(/^.*[\\/](.*)\.store\.js$/i, "$1");
  //   modules[moduleName] = requireService(filename).default();
  // });
  return new Vuex.Store({ modules, ...storeRoot });
}
