// import { ROOT_ACTIONS, ROOT_GETTERS, ROOT_MUTATIONS } from "../store";
import { ObjectsEqual } from "../../utils/ObjectsEqual";

export const DAYS_GETTERS = {
  LAST_AVAILABLE_DAY: "LAST_AVAILABLE_DAY",
  DAYS: "DAYS"
};

export const DAYS_MUTATIONS = {
  SET_DAYS: "SET_DAYS",
  REMOVE_DAY: "REMOVE_DAY",
  UPDATE_DAY: "UPDATE_DAY",
  CREATE_DAY: "CREATE_DAY"
};

export const DAYS_ACTIONS = {
  REMOVE_DAY: "REMOVE_DAY",
  UPDATE_DAY: "UPDATE_DAY",
  CREATE_DAY: "CREATE_DAY"
};

export const days = () => {
  return {
    state: {
      days: []
    },
    getters: {
      [DAYS_GETTERS.LAST_AVAILABLE_DAY](state) {
        return [...state.days].sort((a, b) => a.date - b.date).pop();
      },
      [DAYS_GETTERS.DAYS]: state => state.days
    },
    mutations: {
      [DAYS_MUTATIONS.SET_DAYS](store, days) {
        store.days = days;
      },
      [DAYS_MUTATIONS.REMOVE_DAY](store, day) {
        store.days.splice(
          store.days.findIndex(e => ObjectsEqual(e, day)),
          1
        );
      },
      [DAYS_MUTATIONS.UPDATE_DAY](store, { targetDate, day }) {
        store.days.splice(
          store.days.findIndex(e => e.OpDate === targetDate),
          1,
          day
        );
      },
      [DAYS_MUTATIONS.CREATE_DAY](store, day) {
        store.days.unshift(day);
      }
    },
    actions: {
      [DAYS_ACTIONS.REMOVE_DAY]({ commit }, day) {
        commit(DAYS_MUTATIONS.REMOVE_DAY, day);
      },
      [DAYS_ACTIONS.UPDATE_DAY]({ commit }, { targetDate, day }) {
        commit(DAYS_MUTATIONS.UPDATE_DAY, { targetDate, day });
      },
      [DAYS_ACTIONS.CREATE_DAY]({ commit }, day) {
        commit(DAYS_MUTATIONS.CREATE_DAY, day);
      }
    }
  };
};
