import { createAccount } from "../../models/account";
import { TRANSACTIONS_GETTERS } from "./transactions.store";

export const ACCOUNTS_GETTERS = {
  ACCOUNTS: "ACCOUNTS",
  ACCOUNT_NUMBERS: "ACCOUNT_NUMBERS",
  ACCOUNT_BALANCE: "ACCOUNT_BALANCE",
  ACCOUNT_BY_NUMBER: "ACCOUNT_BY_NUMBER"
};

export const ACCOUNTS_MUTATIONS = {
  SET_ACCOUNTS: "SET_ACCOUNTS",
  REMOVE_ACCOUNT: "REMOVE_ACCOUNT",
  UPDATE_ACCOUNT: "UPDATE_ACCOUNT",
  CREATE_ACCOUNT: "CREATE_ACCOUNT"
};

export const ACCOUNTS_ACTIONS = {
  REMOVE_ACCOUNT: "REMOVE_ACCOUNT",
  UPDATE_ACCOUNT: "UPDATE_ACCOUNT",
  CREATE_ACCOUNT: "CREATE_ACCOUNT"
};

export const accounts = () => {
  return {
    state: {
      accounts: []
    },
    getters: {
      [ACCOUNTS_GETTERS.ACCOUNTS]: state => state.accounts,

      [ACCOUNTS_GETTERS.ACCOUNT_NUMBERS]: state =>
        state.accounts.map(e => e.Acct),
      [ACCOUNTS_GETTERS.ACCOUNT_BY_NUMBER]: state => number =>
        state.accounts.find(e => e.Acct === number),

      [ACCOUNTS_GETTERS.ACCOUNT_BALANCE]: (
        state,
        getters,
        rootState,
        rootGetters
      ) => (date, accounts) => {
        if (typeof accounts === "undefined") accounts = state.accounts;
        return accounts.map(acc => {
          return rootGetters[TRANSACTIONS_GETTERS.TRANSACTIONS]
            .filter(
              doc =>
                (doc.AcctCr === acc.Acct || doc.AcctDB === acc.Acct) &&
                doc.date <= date
            )
            .reduce((a, doc) => {
              if (doc.AcctCr === a.Acct) a.Ost -= doc.Amount;
              else a.Ost += doc.Amount;
              return a;
            }, createAccount(acc));
        });
      }
    },
    mutations: {
      [ACCOUNTS_MUTATIONS.SET_ACCOUNTS](store, accounts) {
        store.accounts = accounts;
      },
      [ACCOUNTS_MUTATIONS.REMOVE_ACCOUNT](store, account) {
        store.accounts.splice(
          store.accounts.findIndex(e => e.Acct === account.Acct),
          1
        );
      },
      [ACCOUNTS_MUTATIONS.UPDATE_ACCOUNT](store, { targetNumber, account }) {
        store.accounts.splice(
          store.accounts.findIndex(e => e.Acct === targetNumber),
          1,
          account
        );
      },
      [ACCOUNTS_MUTATIONS.CREATE_ACCOUNT](store, account) {
        store.accounts.unshift(account);
      }
    },
    actions: {
      [ACCOUNTS_ACTIONS.REMOVE_ACCOUNT]({ commit }, account) {
        commit(ACCOUNTS_MUTATIONS.REMOVE_ACCOUNT, account);
      },
      [ACCOUNTS_ACTIONS.UPDATE_ACCOUNT]({ commit }, { targetNumber, account }) {
        commit(ACCOUNTS_MUTATIONS.UPDATE_ACCOUNT, { targetNumber, account });
      },
      [ACCOUNTS_ACTIONS.CREATE_ACCOUNT]({ commit }, account) {
        commit(ACCOUNTS_MUTATIONS.CREATE_ACCOUNT, account);
      }
    }
  };
};
