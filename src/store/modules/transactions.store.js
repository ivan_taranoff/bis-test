// import { ROOT_ACTIONS, ROOT_GETTERS, ROOT_MUTATIONS } from "../store";
import { ObjectsEqual } from "../../utils/ObjectsEqual";
import { ACCOUNTS_GETTERS } from "./accounts.store";

export const TRANSACTIONS_GETTERS = {
  TRANSACTIONS: "TRANSACTIONS",
  TRANSACTIONS_BY_DATE: "TRANSACTIONS_BY_DATE",
  TRANSACTIONS_BY_ACCOUNT: "TRANSACTIONS_BY_ACCOUNT",
  TRANSACTION_INFO: "TRANSACTION_INFO"
};

export const TRANSACTIONS_MUTATIONS = {
  SET_DOCUMENTS: "SET_DOCUMENTS",
  REMOVE_TRANSACTION: "REMOVE_TRANSACTION",
  UPDATE_TRANSACTION: "UPDATE_TRANSACTION",
  CREATE_TRANSACTION: "CREATE_TRANSACTION"
};

export const TRANSACTIONS_ACTIONS = {
  REMOVE_TRANSACTION: "REMOVE_TRANSACTION",
  UPDATE_TRANSACTION: "UPDATE_TRANSACTION",
  CREATE_TRANSACTION: "CREATE_TRANSACTION"
};

export const transactions = () => {
  return {
    state: {
      transactions: []
    },
    getters: {
      [TRANSACTIONS_GETTERS.TRANSACTIONS]: state => state.transactions,

      [TRANSACTIONS_GETTERS.TRANSACTIONS_BY_DATE]: state => date => {
        return state.transactions.filter(
          doc => doc.date.valueOf() === date.valueOf()
        );
      },

      [TRANSACTIONS_GETTERS.TRANSACTIONS_BY_ACCOUNT]: state => accNr =>
        state.transactions.filter(
          doc => doc.AcctCr === accNr || doc.AcctDB === accNr
        ),
      [TRANSACTIONS_GETTERS.TRANSACTION_INFO]: (
        state,
        getters,
        rootState,
        rootGetters
      ) => transaction => {
        // const { AcctCr, AcctDB } = transaction;
        const accounts = rootGetters[ACCOUNTS_GETTERS.ACCOUNTS].filter(
          e => transaction.AcctCr === e.Acct || transaction.AcctDB === e.Acct
        );
        return getters[ACCOUNTS_GETTERS.ACCOUNT_BALANCE](
          transaction.date,
          accounts
        );
      }
    },
    mutations: {
      [TRANSACTIONS_MUTATIONS.SET_DOCUMENTS](store, transactions) {
        store.transactions = transactions;
      },
      [TRANSACTIONS_MUTATIONS.REMOVE_TRANSACTION](store, transaction) {
        store.transactions.splice(
          store.transactions.findIndex(e => ObjectsEqual(e, transaction)),
          1
        );
      },
      [TRANSACTIONS_MUTATIONS.UPDATE_TRANSACTION](
        store,
        { target, transaction }
      ) {
        store.transactions.splice(
          store.transactions.findIndex(e => ObjectsEqual(e, target)),
          1,
          transaction
        );
      },
      [TRANSACTIONS_MUTATIONS.CREATE_TRANSACTION](store, transaction) {
        store.transactions.unshift(transaction);
      }
    },
    actions: {
      [TRANSACTIONS_ACTIONS.REMOVE_TRANSACTION]({ commit }, transaction) {
        commit(TRANSACTIONS_MUTATIONS.REMOVE_TRANSACTION, transaction);
      },
      [TRANSACTIONS_ACTIONS.UPDATE_TRANSACTION](
        { commit },
        { target, transaction }
      ) {
        commit(TRANSACTIONS_MUTATIONS.UPDATE_TRANSACTION, {
          target,
          transaction
        });
      },
      [TRANSACTIONS_ACTIONS.CREATE_TRANSACTION]({ commit }, transaction) {
        commit(TRANSACTIONS_MUTATIONS.CREATE_TRANSACTION, transaction);
      }
    }
  };
};
