import Vue from "vue";
import Router from "vue-router";
import Accounts from "../views/Accounts";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      redirect: "accounts"
    },
    {
      path: "/accounts/:account?",
      name: "accounts",
      component: Accounts,
      props: true
    },
    {
      path: "/days/:day?",
      name: "days",
      props: true,
      component: () => import(/* webpackChunkName: "days" */ "@/views/DaysView")
    },
    {
      path: "/operations/:transaction?",
      name: "operations",
      props: true,
      component: () =>
        import(/* webpackChunkName: "operations" */ "@/views/Operations")
    }
  ]
});
