export const TableActions = {
  methods: {
    showDetail(row) {
      if (this.master) this.$emit("show-detail", row);
    }
  }
};
