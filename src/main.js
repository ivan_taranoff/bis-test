import Vue from "vue";
import "./plugins/axios";
import App from "./App.vue";
import router from "./router";
// import store from "./store/store";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import { createStore } from "./store/store";
import elementLangEn from "element-ui/lib/locale/lang/en";
// import elementLocale from "element-ui/lib/locale";
// elementLocale.use(elementLangEn);

Vue.config.productionTip = false;

Vue.use(ElementUI, { elementLangEn });

const store = createStore();

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
